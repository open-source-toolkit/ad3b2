# C# WinForm 串口助手及波形显示上位机

## 简介

本仓库提供了一个使用C# WinForm编写的简单串口助手及波形显示上位机程序。该程序可以帮助用户通过串口与外部设备进行通信，并实时显示接收到的数据波形。

## 功能特点

- **串口通信**：支持常见的串口通信设置，如波特率、数据位、停止位、校验位等。
- **数据接收与显示**：能够实时接收串口数据，并在界面上显示接收到的数据。
- **波形显示**：支持将接收到的数据以波形图的形式显示，方便用户直观地观察数据变化。
- **简单易用**：界面简洁，操作方便，适合初学者学习和使用。

## 使用说明

1. **打开程序**：运行程序后，界面会显示串口设置和波形显示区域。
2. **配置串口**：在串口设置区域选择正确的串口号，并配置相应的通信参数（波特率、数据位、停止位、校验位）。
3. **打开串口**：点击“打开串口”按钮，程序将尝试与选定的串口建立连接。
4. **接收数据**：一旦串口连接成功，程序将开始接收来自串口的数据，并在波形显示区域实时绘制波形。
5. **关闭串口**：当不再需要通信时，点击“关闭串口”按钮以断开连接。

## 注意事项

- 请确保在打开串口之前，已经正确连接了外部设备，并且设备已经正确配置。
- 如果波形显示不正常，请检查串口数据格式是否与程序设置一致。

## 贡献

欢迎大家提出改进建议或提交代码改进。如果您有任何问题或建议，请在仓库中提交Issue。

## 许可证

本项目采用MIT许可证，详情请参阅LICENSE文件。